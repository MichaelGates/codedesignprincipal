package com.michael.mycalendar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author michaelgates
 */
public class MyCalendar {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Date date;
        do {
            System.out.print("Input Date(yyyy-MM-dd): ");
            String inputString = br.readLine();
            date = transformDateStringToDate(inputString);
        } while (date == null);

        System.out.println(getDayOfMonthBySunday(getTheFirstDayOfTheWeek(date)));
    }

    public static Date getTheFirstDayOfTheWeek(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        calendar.add(Calendar.DAY_OF_MONTH, 1 - day);
        return calendar.getTime();
    }

    public static String printTitle() {
        return "Sun\tMon\tTue\tWed\tThu\tFri\tSat";
    }

    public static String getDayOfMonthBySunday(Date date) {
        StringBuffer sb = new StringBuffer();
        sb.append(MyCalendar.printTitle()).append(System.lineSeparator());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        sb.append(calendar.get(Calendar.DAY_OF_MONTH));
        for (int i=1; i<7; i++) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            sb.append("\t").append(calendar.get(Calendar.DAY_OF_MONTH));
        }
        return sb.toString();
    }

    public static Date transformDateStringToDate(String dateStr) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
        return date;
    }
}
