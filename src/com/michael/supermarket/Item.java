package com.michael.supermarket;

public class Item {

    /**
     * 数量
     */
    private int quantity;
    /**
     * 单价
     */
    private double unitPrice;
    /**
     * 商品类型，枚举类型
     */
    private ItemTypeEnum itemType;

    public Item(double unitPrice, int quantity) {
        this.unitPrice = unitPrice;
        this.quantity = quantity;
    }

    public Item(ItemTypeEnum itemType, double unitPrice, int quantity) {
        this(unitPrice, quantity);
        this.itemType = itemType;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public ItemTypeEnum getItemType() {
        return itemType;
    }
}
