package com.michael.supermarket;

/**
 * @author michaelgates
 */

public enum ItemTypeEnum {
    DRINK, MEAT, ELECTRONICS
}
