package com.michael.supermarket;

public class PercentageDiscount {
    /**
     * 商品类型，枚举类型
     */
    private ItemTypeEnum itemType;
    /**
     * 折扣率
     */
    private double discountRate;

    public PercentageDiscount(ItemTypeEnum itemType, double discountRate) {
        this.itemType = itemType;
        this.discountRate = discountRate;
    }

    public double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }

    public ItemTypeEnum getItemType() {
        return itemType;
    }

    public void setItemType(ItemTypeEnum itemType) {
        this.itemType = itemType;
    }
}
