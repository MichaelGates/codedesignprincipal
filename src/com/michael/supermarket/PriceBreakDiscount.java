package com.michael.supermarket;

public class PriceBreakDiscount {
    /**
     * 商品类型
     */
    private ItemTypeEnum itemType;
    /**
     * 满减门槛
     */
    private double threshold;
    /**
     * 满减金额
     */
    private double discount;

    public PriceBreakDiscount(double threshold, double discount) {
        this.threshold = threshold;
        this.discount = discount;
    }

    public PriceBreakDiscount(ItemTypeEnum itemType, double threshold, double discount) {
        this(threshold, discount);
        this.itemType = itemType;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public ItemTypeEnum getItemType() {
        return itemType;
    }
}
