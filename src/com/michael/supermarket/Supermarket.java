package com.michael.supermarket;

import java.util.HashMap;
import java.util.List;

/**
 * @author michaelgates
 */
public class Supermarket {

    public static double calcAmount(List<Item> items) {
        double total = 0.0;
        for (Item item : items) {
            total += item.getUnitPrice() * item.getQuantity();
        }
        return total;
    }

    public static double calcAmountWithDiscount(List<Item> items, Double discountRate) {
        return calcAmount(items) * discountRate;
    }

    /**
     * 计算含有多个"满减"优惠规则的优惠后金额
     *
     * @param items               商品信息
     * @param priceBreakDiscounts "满减"优惠规则数组，要求门槛金额低的在数组中靠前，金额高的在后
     * @return 满减优惠后的购物车总金额
     */
    public static double calcAmountWithPriceBreakDiscount(List<Item> items, List<PriceBreakDiscount> priceBreakDiscounts) {
        double totalPrice = calcAmount(items);
        for (int i = priceBreakDiscounts.size() - 1; i >= 0; i--) {
            PriceBreakDiscount priceBreakDiscount = priceBreakDiscounts.get(i);
            if (totalPrice > priceBreakDiscount.getThreshold()) {
                totalPrice -= priceBreakDiscount.getDiscount();
                break;
            }
        }
        return totalPrice;
    }

    public static double calcAmountWithPercentageDiscount(List<Item> items, PercentageDiscount percentageDiscount) {
        double total = 0.0;
        for (Item item : items) {
            if (percentageDiscount.getItemType().equals(item.getItemType())) {
                total += item.getUnitPrice() * item.getQuantity() * percentageDiscount.getDiscountRate();
            } else {
                total += item.getUnitPrice() * item.getQuantity();
            }
        }
        return total;
    }

    /**
     * 根据百分百折扣和满减折扣信息，计算商品总价
     * @param items 购物车商品薪酬
     * @param percentageDiscount 百分比折扣信息
     * @param priceBreakDiscounts 满减折扣信息数组，要求门槛金额低的在数组中靠前，金额高的在后
     * @return 优惠后的商品总价
     */
    public static double calcAmountWithPercentageDiscountAndPriceBreakDiscount(List<Item> items, PercentageDiscount percentageDiscount, List<PriceBreakDiscount> priceBreakDiscounts) {
        HashMap<ItemTypeEnum, Double> typeTotal = new HashMap<ItemTypeEnum, Double>(ItemTypeEnum.values().length);
        for (ItemTypeEnum itemType : ItemTypeEnum.values()) {
            typeTotal.put(itemType, 0.0);
        }
        for (Item item : items) {
            typeTotal.put(item.getItemType(), typeTotal.get(item.getItemType()) + item.getUnitPrice() * item.getQuantity());
        }

        for (ItemTypeEnum itemType : ItemTypeEnum.values()) {
            if (itemType.equals(percentageDiscount.getItemType())) {
                typeTotal.put(percentageDiscount.getItemType(),typeTotal.get(percentageDiscount.getItemType())*percentageDiscount.getDiscountRate());
            }
        }

        for (int i = priceBreakDiscounts.size() - 1; i >= 0; i--) {
            PriceBreakDiscount priceBreakDiscount = priceBreakDiscounts.get(i);
            if (typeTotal.get(priceBreakDiscount.getItemType()) > priceBreakDiscount.getThreshold()) {
                Double discountedTypeTotalPrice = typeTotal.get(priceBreakDiscount.getItemType()) - priceBreakDiscount.getDiscount();
                typeTotal.put(priceBreakDiscount.getItemType(),discountedTypeTotalPrice);
                break;
            }
        }

        double total = 0.0;
        for (ItemTypeEnum itemType : ItemTypeEnum.values()) {
            total += typeTotal.get(itemType);
        }
        return total;
    }
}
