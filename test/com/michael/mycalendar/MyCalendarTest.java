package com.michael.mycalendar;

import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyCalendarTest {

    @Test
    public void SundayShouldReturnItself() throws ParseException {
        Date originDate = new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-23");
        Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-23"), MyCalendar.getTheFirstDayOfTheWeek(originDate));
    }

    @Test
    public void MondayShouldReturnTheDayBefore() throws ParseException {
        Date originDate = new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-24");
        Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-23"), MyCalendar.getTheFirstDayOfTheWeek(originDate));
    }

    @Test
    public void TuesdayShouldReturn2DaysBefore() throws ParseException {
        Date originDate = new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-25");
        Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-23"), MyCalendar.getTheFirstDayOfTheWeek(originDate));
    }

    @Test
    public void WednesdayShouldReturn3DaysBefore() throws ParseException {
        Date originDate = new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01");
        Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("2019-12-29"), MyCalendar.getTheFirstDayOfTheWeek(originDate));
    }

    @Test
    public void DisplayTheTitle() {
        Assert.assertEquals("Sun\tMon\tTue\tWed\tThu\tFri\tSat",MyCalendar.printTitle());
    }

    @Test
    public void PrintTheTitleAndDayOfMonthBySunday() throws ParseException {
        StringBuffer sb = new StringBuffer();
        sb.append("Sun\tMon\tTue\tWed\tThu\tFri\tSat").append(System.lineSeparator());
        sb.append("24\t25\t26\t27\t28\t29\t1");
        Date originDate = new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-24");
        Assert.assertEquals(sb.toString(), MyCalendar.getDayOfMonthBySunday(originDate));
    }

    @Test
    public void CorrectDateStringShouldReturnDate() throws ParseException {
        Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-24"), MyCalendar.transformDateStringToDate("2020-02-24"));
    }

    @Test
    public void WrongDateStringShouldReturnNull() throws ParseException {
        Assert.assertEquals(null, MyCalendar.transformDateStringToDate("20201232"));
    }
}