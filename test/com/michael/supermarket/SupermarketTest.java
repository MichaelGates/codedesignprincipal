package com.michael.supermarket;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class SupermarketTest {

    @Test
    public void emptyShouldReturnZero() {
        List<Item> items = new ArrayList<Item>();
        items.add(new Item(0.0 ,0));
        Assert.assertEquals(0.0, Supermarket.calcAmount(items),1e-10);
    }

    @Test
    public void oneItemShouldReturnUnitPrice() {
        List<Item> items = new ArrayList<Item>();
        items.add(new Item(15.0 ,1));
        Assert.assertEquals(15.0, Supermarket.calcAmount(items),1e-10);
    }

    @Test
    public void multiplyNumberForOneItemShouldReturnTotalPrice() {
        List<Item> items = new ArrayList<Item>();
        items.add(new Item(5.0 ,2));
        Assert.assertEquals(10.0, Supermarket.calcAmount(items),1e-10);
    }

    @Test
    public void twoItemsShouldReturnTotalPrice() {
        Item item = new Item(5.0 ,2);
        List<Item> items = new ArrayList<Item>();
        items.add(item);
        item = new Item(25.0,2);
        items.add(item);
        Assert.assertEquals(60.0, Supermarket.calcAmount(items),1e-10);
    }

    @Test
    public void allItemsShouldReturnTotalPrice() {
        List<Item> items = initialShoppingCar();
        Assert.assertEquals(225.0, Supermarket.calcAmount(items),1e-10);
    }

    @Test
    public void twentyDiscountShouldReturnDiscountedTotalPrice() {
        List<Item> items = initialShoppingCar();
        Double discountRate = 0.8;
        Assert.assertEquals(180.0, Supermarket.calcAmountWithDiscount(items, discountRate),1e-10);
    }

    @Test
    public void minusFiveWhenReachFiftyShouldReturnDiscountedTotalPrice() {
        List<Item> items = initialShoppingCar();
        PriceBreakDiscount priceBreakDiscount = new PriceBreakDiscount(50.0,5.0);
        List<PriceBreakDiscount> priceBreakDiscounts = new ArrayList<PriceBreakDiscount>();
        priceBreakDiscounts.add(priceBreakDiscount);
        Assert.assertEquals(220.0, Supermarket.calcAmountWithPriceBreakDiscount(items, priceBreakDiscounts),1e-10);
    }

    @Test
    public void twoPriceBreakDiscountPolicyShouldReturnDiscountedTotalPrice() {
        List<Item> items = initialShoppingCar();
        PriceBreakDiscount priceBreakDiscount = new PriceBreakDiscount(50.0,5.0);
        List<PriceBreakDiscount> priceBreakDiscounts = new ArrayList<PriceBreakDiscount>();
        priceBreakDiscounts.add(priceBreakDiscount);
        priceBreakDiscount = new PriceBreakDiscount(100.0,15.0);
        priceBreakDiscounts.add(priceBreakDiscount);
        Assert.assertEquals(210.0, Supermarket.calcAmountWithPriceBreakDiscount(items, priceBreakDiscounts),1e-10);
    }

    @Test
    public void drinkDiscountShouldReturnDiscountedTotalPrice() {
        List<Item> items = initialShoppingCar();
        PercentageDiscount percentageDiscount = new PercentageDiscount(ItemTypeEnum.DRINK, 0.8);
        Assert.assertEquals(220.0, Supermarket.calcAmountWithPercentageDiscount(items, percentageDiscount), 1e-10);
    }

    @Test
    public void drinkDiscountAndMeatPriceBreakDiscountShouldReturnDiscountedTotalPrice() {
        List<Item> items = initialShoppingCar();
        PercentageDiscount percentageDiscount = new PercentageDiscount(ItemTypeEnum.DRINK, 0.8);

        List<PriceBreakDiscount> priceBreakDiscounts = new ArrayList<PriceBreakDiscount>();
        PriceBreakDiscount priceBreakDiscount = new PriceBreakDiscount(ItemTypeEnum.MEAT,20.0, 2.0);
        priceBreakDiscounts.add(priceBreakDiscount);
        priceBreakDiscount = new PriceBreakDiscount(ItemTypeEnum.MEAT, 60.0, 8.0);
        priceBreakDiscounts.add(priceBreakDiscount);

        Assert.assertEquals(212.0,
                Supermarket.calcAmountWithPercentageDiscountAndPriceBreakDiscount(
                        items,
                        percentageDiscount,
                        priceBreakDiscounts),
                1e-10);
    }

    private List<Item> initialShoppingCar() {
        List<Item> items = new ArrayList<Item>();
        Item item = null;
        item = new Item(ItemTypeEnum.DRINK, 15.0, 1);
        items.add(item);
        item = new Item(ItemTypeEnum.DRINK, 5.0, 2);
        items.add(item);
        item = new Item(ItemTypeEnum.MEAT, 25.0, 2);
        items.add(item);
        item = new Item(ItemTypeEnum.MEAT,10.0, 5);
        items.add(item);
        item = new Item(ItemTypeEnum.ELECTRONICS,100.0, 1);
        items.add(item);
        return items;
    }

}
